package Static;

class Soma{
    // Método statico, posso chamar ele sem instanciar minha classe Soma;
    public static int resultado(int num1, int num2){
        return (num1 + num2);
    }
}

public class TestaSomaEstatica {

    //
    public static void main(String[] args) {

        System.out.println(Soma.resultado(2,3));


    }
}



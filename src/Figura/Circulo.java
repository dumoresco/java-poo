package Figura;


public class Circulo extends Figura{


    // Declara atributo raio
    private double raio;

    // Declara construtor padrão
    public Circulo(){}

   // Declara construtor recebendo valor de cor e raio
    public Circulo(String cor, double raio){
        super(cor);
        this.raio = raio;
    }

    // Get para retornar o valor de raio
    public double getRaio(){
        return raio;
    }

    // Set para definir o valor de raio
    public void setRaio(double raio){
        this.raio = raio;
    }

    // Sobrescreve método abstrato para calcular a área
    @Override
    public double area() {
        return raio * 3.14 * 2;
    }

    @Override
    public String toString(){
        return "Circulo: [Raio: " + raio + " Cor: " + super.getCor() + " Area: " + area() + "]";
    }

   
}

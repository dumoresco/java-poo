package Figura;

public class Triangulo extends Retangulo{

    // Declara atributos de base e altura
    double base;
    private double altura;


    // Declara construtor padrão ( vazio )
    public Triangulo(){}

    // Declara construtor recebendo os valores de base, altura e cor
    public Triangulo(double base, double altura, String cor){
        super(base,altura,cor);
    }


    // Getters para retornar os valores dos atributos
    public double getBase(){
        return base;
    }
    public double getAltura(){
        return altura;
    }

    // Settes para definir os valores dos atributos
    public void setBase(double base){ this.base = base; }
    public void setAltura(double altura){
        this.altura = altura;
    }



    // Sobrescreve método abstrato para calcular a área
    public double exibir(){
        return teste;
    }
    @Override
    public double area(){
        return (base * altura)/2;
    }

    @Override
    public String toString(){
        return "Triangulo [Base: " + base + ", Altura: " + altura + " Cor: " + super.getCor() + " Area: " + area() + " ]";

    }
}

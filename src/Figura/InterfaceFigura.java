package Figura;

// Declara a interface, serve como um "contrato",
public interface InterfaceFigura{
     Double areaValue= Double.valueOf(0);

     public double setArea();

     // Implementação padrão para um método de interface
     // no caso das classes que implementam a interface não implementarem o método
     public default double setArea2(){
         return areaValue;
     };


}

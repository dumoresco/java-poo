package Figura;

public class Retangulo extends Figura{

    // Declara atributos de base e altura
    private double base;
    public static double teste;
    private double altura;

    // Declara construtor padrão ( vazio )
    public Retangulo() {

    }

    // Declara construtor recebendo o valor de base, altura e cor
    public Retangulo(double base, double altura, String cor) {
        super(cor);
        this.base = base;
        this.altura = altura;

    }

    // Getters para retornar o valor dos atributos
    public double getBase() {
        return base;
    }
    public double getAltura() {
        return altura;
    }


    // Setters definir o valor dos atributos
    public void setBase(double base) { this.base = base; }
    public void setAltura(double altura) { this.altura = altura; }

    // Sobrescrevendo o método de área da minha classe abstrata Figura
    @Override
    public double area() {
        return base * altura;
    }

    // Sobrescrevendo o método toString da classe mãe Object
    @Override
    public String toString() {
        return "Retangulo [Base: " + base + ", Altura: " + altura + " Cor: " + super.getCor() + " Area: " + area()
                + " ]";
    }


}

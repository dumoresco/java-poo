package Figura;


// Classe abstrata, não pode ser instanciada, serve para
// ser herdada, conjunto de código comum para outras classes

public abstract class Figura {

    // Declara atributo de cor
    private String cor = "Amarela";

    // Declara construtor padrão
    public Figura() {}


    // Declara construtor para passar/ receber valor de cor
    public Figura(String cor) { this.cor = cor; }

    // Set para definir valor de cor
    public void setCor(String cor) {this.cor = cor;}

    // Get para retornar valor de cor
    public String getCor() {return cor;}

    // @Override serve garantir que você está sobrescrevendo um método e não criando
    // um novo.
    @Override
    public String toString() {
        return "Figura [cor=" + cor + "]";
    }

    // Método abstrato vazio
    public abstract double area();

}

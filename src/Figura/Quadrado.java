package Figura;

public class Quadrado extends Figura {

    // Declara atributo de lado
    protected double lado;

    // Declara construtor padrão ( vazio )
    public Quadrado() {
    }


    // Declara construtor recebendo o valro de cor e lado.
    public Quadrado(String cor, double lado) {
        super(cor);
        this.lado = lado;
    }


    // Getter para retornar o valor do atributo lado
    protected double getLado() {
        return lado;
    }

    // Set para definir o valor do atributo lado
    public void setLado(double lado) {
        this.lado = lado;
    }

    // Sobrescreve método abstrato do calculo de área
    @Override
    public double area() {
        return lado * lado;
    }


    @Override
    public String toString() {
        return "Quadrado [Lados: " + lado + " Cor: " + super.getCor() + " Area: " + area() + " ]";
    }
}
